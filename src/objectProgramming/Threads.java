package objectProgramming;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

class Counter {
    private int value;

    public void increment() {
        synchronized (this) {
            value += 1;
        }
    }

    public int getValue() {
        return value;
    }
}

public class Threads {
    private static volatile boolean isDone = false;

        public void run () {

            Thread stars = new Thread(() -> {
                Counter counter = new Counter();
                LocalDateTime start = LocalDateTime.now();
                while (!isDone) {
                    try {
                        while (counter.getValue() <= 100) {
                            System.out.println("\r" + "*".repeat(counter.getValue()));
                            counter.increment();
                            if(counter.getValue() < 100)  Thread.sleep((long) (100 * Math.random()));
                        }

                        isDone = true;
                        long durationInSeconds = ChronoUnit.SECONDS.between(start, LocalDateTime.now());
                        System.out.println("\n" + Thread.currentThread().getName() + " thread finished after " + durationInSeconds + " seconds");
                        System.exit(0);

                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });

            Thread pluses = new Thread(() -> {
                Counter counter = new Counter();
                LocalDateTime start = LocalDateTime.now();
                while (!isDone) {
                    try {
                        while (counter.getValue() <= 100) {
                            System.out.println("\r" + "+".repeat(counter.getValue()));
                            counter.increment();
                            if(counter.getValue() < 100) Thread.sleep((long) (100 * Math.random()));
                        }

                        isDone = true;
                        long durationInSeconds = ChronoUnit.SECONDS.between(start, LocalDateTime.now());
                        System.out.println("\n" + Thread.currentThread().getName() + " thread finished after " + durationInSeconds + " seconds");
                        System.exit(0);

                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });

            Thread hashes = new Thread(() -> {
                Counter counter = new Counter();
                LocalDateTime start = LocalDateTime.now();
                while (!isDone) {
                    try {
                        while (counter.getValue() <= 100) {
                            System.out.println("\r" + "#".repeat(counter.getValue()));
                            counter.increment();
                            if(counter.getValue() < 100) Thread.sleep((long) (100 * Math.random()));
                        }

                        isDone = true;
                        long durationInSeconds = ChronoUnit.SECONDS.between(start, LocalDateTime.now());
                        System.out.println("\n" + Thread.currentThread().getName() + " thread finished after " + durationInSeconds + " seconds");
                        System.exit(0);

                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });

            pluses.start();
            stars.start();
            hashes.start();
            pluses.setName("Pluses");
            stars.setName("Stars");
            hashes.setName("Hashes");
    }
}
