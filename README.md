![Project icon](images/threads-icon.png)

# Threads

Create 3 threads that will print characters (f.e. *, +, #) in the terminal. 
Put the threads to sleep for a random time, then exit the program when any of the threads writes out 100 characters.

**Example:**
```
+++++
**********
###################################
```
## Assessment criteria
- Compliance of the program with the requirements.
- Code compliance with good Java programming conventions and practices.

## Instructions
1. Clone this repository.
2. Run project in IDE.

## Helpful links
- https://www.samouczekprogramisty.pl/watki-w-jezyku-java/
- https://javaconceptoftheday.com/naming-thread-java/
- https://www.w3schools.com/java/java_while_loop.asp
- https://www.baeldung.com/java-wait-and-sleep
- http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
- https://stackoverflow.com/questions/7939802/how-can-i-print-to-the-same-line
- https://stackoverflow.com/questions/19183955/why-we-use-system-out-flush
- https://developer.android.com/reference/java/io/OutputStream#flush%28%29
- https://www.baeldung.com/java-interrupted-exception
- https://docs.oracle.com/javase/tutorial/essential/concurrency/sleep.html
- https://stackoverflow.com/questions/39683952/how-to-stop-a-running-thread-in-java
- https://docs.oracle.com/javase/tutorial/essential/concurrency/interrupt.html
- https://www.geeksforgeeks.org/system-exit-in-java/
- https://stackoverflow.com/questions/5028095/how-to-know-which-thread-completes-execution-first-out-of-two-threads
- https://stackoverflow.com/questions/3381619/in-multithreading-how-to-determine-which-thread-stops-first
- https://coderanch.com/t/389825/java/Printing-line-console
- https://stackoverflow.com/questions/27322707/print-character-multiple-times
- https://www.geeksforgeeks.org/how-to-insert-an-element-at-a-specific-position-in-an-array-in-java/
- https://java2blog.com/print-sequence-3-threads-java/

## License
[MIT](https://choosealicense.com/licenses/mit/)
